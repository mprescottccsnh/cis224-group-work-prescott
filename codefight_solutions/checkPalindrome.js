/* The Journey Begins - Number 3 - Arcade */

function checkPalindrome(inputString) {
    
    var start = 0;
    var end = inputString.length - 1;
    
    while (start <= end) {
        if (inputString[start] != inputString[end])
            return false;
        start++;
        end--;
    }
    
    return true;

}
