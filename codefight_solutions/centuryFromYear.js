/* Journey Begins Number 2 - Arcade */

function centuryFromYear(year) {
    return (year % 100 === 0) ? year/100 : parseInt(year / 100) + 1;
}
