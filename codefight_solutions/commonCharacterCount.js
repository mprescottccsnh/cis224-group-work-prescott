/* Smooth Sailing Problem #10 - Arcade */

var commonCharacterCount = (s1, s2) => {

    var getMap = (str) => {
        var map = new Map();
        for (letter of str) {
            if (map.has(letter)) {
                map.set(letter, map.get(letter) + 1);
            } else {
                map.set(letter, 1);
            }
        }
        return map;
    }

    var count = 0;
    var s1map = getMap(s1);
    var s2map = getMap(s2);
    var s1set = new Set(s1);

    for (letter of s1set) {
        if (s2map.has(letter)) {
            count += (s1map.get(letter) <= s2map.get(letter)) 
                ? s1map.get(letter) 
                : s2map.get(letter);
        }
    }

    return count;
}
