$(function () {
	var clickCount;
	var matches, $matches;
	var monkeyClasses = ["SeeNoEvil", "HearNoEvil", "SpeakNoEvil"];
	var $monkeys;
	var $resetBtn;
	var pair;

	$monkeys = $('#monkeys div');
    $matches = $('#matches');
    $resetBtn = $('#reset');
	
	function init() {
		clickCount = 0;
		pair = [];
		
		if(!localStorage.getItem('matches')) {
			localStorage.setItem('matches', 0);
		}
		
		matches = Number(localStorage.getItem('matches'));
		$matches.text(matches);
		
		$monkeys.each(function () {
			let randomClass;
			let num = monkeyClasses.length;
			randomClass = monkeyClasses[
				Math.floor(Math.random()*num)
			];
			$(this).addClass("_" + randomClass);
		});
	}
	
    function checkForMatch() {
        var classVal = $(this).attr('class');
        
        function updateClass(div) {

            if (classVal[0] === '_') {
                classVal = classVal.slice(1);
                div.attr('class', classVal);
                pair.push(classVal);
                clickCount++;
            }
        }       

        updateClass($(this));

        if (clickCount > 0 && pair[0] === pair[1]) {
			matches++;
			$matches.text(matches);
			localStorage.setItem('matches', matches);
		}

    }
    
    function resetGame() {
        //Remove all class attributes
        $monkeys.each(function () {
            $(this).removeClass();
        });
        
		//Initialize
		init();
    }
    
    $monkeys.on('click', checkForMatch);
    $resetBtn.on('click', resetGame);
	init();
});






